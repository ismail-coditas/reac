import { Component , OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'proj';

  reactiveForm:FormGroup

  ngOnInit(){
    this.reactiveForm= new FormGroup({
      firstname : new FormControl(null),    
      lastname : new FormControl(null),    
      email : new FormControl(null),   
      gender : new FormControl(null),    
      country : new FormControl(null),    
      hobbies : new FormControl(null)    
    })
  }
}
